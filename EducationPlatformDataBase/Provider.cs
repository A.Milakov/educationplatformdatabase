﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EducationPlatformDataBase.Data;
using EducationPlatformDataBase.Data.Enums;
using EducationPlatformDataBase.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace EducationPlatformDataBase;

public class Provider
{
    private readonly ModuleDbContext _dbContext;
    public Provider(ModuleDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    public void ShowCommand()
    {
        Console.WriteLine("\nДля регистрации введите 1\n" +
                          "Для авторизации введите 2\n" +
                          "Для показа собственных курсов введите 3\n" +
                          "Для добавления нового курса введите 4\n" +
                          "Для добавления нового раздела введите 5\n" + 
                          "Для добавления нового урока введите 6\n" + 
                          "Для отображение существующих курсов введите 7\n" +
                          "Для показа списка пользователей введите 8\n" +
                          "Для отображения команд введите 9\n" +
                          "Для завершения программы введите 10\n");
    }

    public async Task ShowUsers()
    {
        var users = await _dbContext.User.ToListAsync() ;
        foreach (var user in users)
        {
            Console.WriteLine($"Имя пользователя: {user.Name}; Логин: {user.Login}");
        }
    }
    public async Task ShowAllCourses(int? userId = null)
    {
        Console.WriteLine();
        List<Course> courses;
        if (userId != null)
            courses = await _dbContext.Course.Where(x=>x.UserId == userId).Include(x => x.User)
                .Include(x => x.Paragraphs).ToListAsync();
        else
            courses = await _dbContext.Course.Include(x => x.User)
                .Include(x => x.Paragraphs).ToListAsync();
        foreach (var course in courses)
        {
            Console.WriteLine($"Id:{course.Id} Course:{course.Name}, Owner:{course.User.Name}");
            if (course.Paragraphs.Count > 0)
            {
                foreach (var paragraph in course.Paragraphs)
                {
                    Console.WriteLine(new String(' ', 4) + $"Id:{paragraph.Id} Paragraph: {paragraph.Name}");
                    var lessons = _dbContext.Lesson.Where(x => x.ParagraphId == paragraph.Id).ToList();
                    if (lessons.Count > 0)
                        foreach (var lesson in lessons)
                            Console.WriteLine(new String(' ', 8) + $"Id:{lesson.Id} Lesson: {lesson.Name}");
                }
            }
        }
        Console.WriteLine();
    }
    private async Task ShowCourses(int userId)
    {
        var courses = await _dbContext.Course
            .Include(x=>x.User)
            .Where(u=>u.User.Id==userId)
            .ToListAsync();
        foreach (var course in courses)
            Console.WriteLine($"Id:{course.Id}: Course:{course.Name}, Creater:{course.User.Name}");
    }

    public async Task ShowMyCources(int userId)
    {
        await ShowAllCourses(userId);
    }
    private async Task ShowCoursePagraphs(int courseId)
    {
        var paragraphs = await _dbContext.Paragraph.Where(p => p.CourseId == courseId).ToListAsync();
        Console.WriteLine();
        foreach (var paragraph in paragraphs)
            Console.WriteLine($"Id:{paragraph.Id} Paragraph: {paragraph.Name}");
        Console.WriteLine();
    }
    public async  Task<RegistrationResult> Registration(string name, string login)
    {
        if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(login))
            return RegistrationResult.IncorrectData;
        var uniqueUser = await _dbContext.User.FirstOrDefaultAsync(x => x.Login == login);
        if (uniqueUser != null)
            return RegistrationResult.NotUniqueLogin;
        
        await _dbContext.User.AddAsync(new User {Login = login, Name = name});
        await _dbContext.SaveChangesAsync();
        return RegistrationResult.Success;
    }

    public async Task<User> Authorization(string login)
    {
        if (string.IsNullOrEmpty(login))
            return null;
        return await _dbContext.User.FirstOrDefaultAsync(x => x.Login == login);
    }
    public async Task AddCourse(User user)
    {
        Console.Write("Введите название курса: ");
        string courseName = Console.ReadLine();
        if (string.IsNullOrEmpty(courseName))
        {
            Console.WriteLine("Название курса не может быть пустым");
            return;
        }

        var courseExist = await _dbContext.Course.AnyAsync(x => x.Name == courseName);
        if (courseExist)
        {
            Console.WriteLine("Курс с таким названием уже существует");
            return;
        }
        Console.Write("Введите описание курса: ");
        var description = Console.ReadLine();

        Course newCourse = new Course
        {
            Name = courseName,
            User = user,
            Description = description
        };
        await _dbContext.Course.AddAsync(newCourse);
        await _dbContext.SaveChangesAsync();
    }
    public async Task AddParagraph(User user)
    {
        Console.WriteLine("Для добавления раздела выберите курс");
        await ShowCourses(user.Id);

        var courseId = ValidationCourseInput();
        if (courseId == null) 
            return;
        
        var selectedCourse = await _dbContext.Course
            .FirstOrDefaultAsync(x => x.Id == courseId && x.User == user);
        if (selectedCourse == null)
        {
            Console.WriteLine("Курс с таким идентификатором не найден");
            return;
        }
        
        Console.Write("Введите название раздела: ");
        string paragraphName = Console.ReadLine();
        if (string.IsNullOrEmpty(paragraphName))
        {
            Console.WriteLine("Название курса не может быть пустым");
            return;
        }
        Console.Write("Введите описание курса: ");
        string description = Console.ReadLine();
        Paragraph newParagraph = new Paragraph()
        {
            Course = selectedCourse,
            Name = paragraphName,
            Description = description
        };
        await _dbContext.Paragraph.AddAsync(newParagraph);
        await _dbContext.SaveChangesAsync();
    }

    private int? ValidationCourseInput()
    {
        Console.Write("Выберите курс: ");
        string strCourseId = Console.ReadLine();
        if (string.IsNullOrEmpty(strCourseId))
        {
            Console.WriteLine("Курс должен быть выбран");
            return null;
        }
        var parseResult = int.TryParse(strCourseId, out int courseId);
        if (!parseResult)
        {
            Console.WriteLine("Введенное значение не является числом");
            return null;
        }
        return courseId;
    }
    private int? ValidationParagraphInput()
    {
        Console.Write("Выберите раздел: ");
        string strPapagraphId = Console.ReadLine();
        if (string.IsNullOrEmpty(strPapagraphId))
        {
            Console.WriteLine("Раздел должен быть выбран");
            return null;
        }
        var parseResult = int.TryParse(strPapagraphId, out int paragraphId);
        if (!parseResult)
        {
            Console.WriteLine("Введенное значение не является числом");
            return null;
        }
        return paragraphId;
    }
    public async Task<bool> CanAddPagraph(User user)
    {
        return await _dbContext.Course.AnyAsync(x => x.User == user);
    }
    public async  Task<bool> CanAddLesson(User user)
    {
        return await _dbContext.Course.Where(x => x.User == user).AnyAsync(c => c.Paragraphs.Count > 0);
    }
    public async Task AddLesson(User user)
    {
        Console.WriteLine("Для добавления урока необходимо выбрать курс и раздел");
        await ShowMyCources(user.Id);
        var selectedCourseId = ValidationCourseInput();
        if (!selectedCourseId.HasValue) 
            return;
        await ShowCoursePagraphs(selectedCourseId.Value);
        var selectedParagraphId = ValidationParagraphInput();
        if (!selectedParagraphId.HasValue) 
            return;
        var selectedParagraph = await _dbContext.Paragraph
            .FirstOrDefaultAsync(x => x.Id == selectedParagraphId && x.CourseId==selectedCourseId);
        if (selectedParagraph == null)
        {
            Console.WriteLine("Раздела с выбранным иденифитаором не сущесвует в рамках данного курса");
            return;
        }
        Console.Write("Введите название урока: ");
        var lessonName = Console.ReadLine();
        if (string.IsNullOrEmpty(lessonName))
        {
            Console.WriteLine("Название курса не может быть пустым");
            return;
        }
        Console.Write("Введите описание урока: ");
        var description = Console.ReadLine();
        var newLesson = new Lesson()
        {
            Name = lessonName,
            Description = description,
            Paragraph = selectedParagraph
        };
        await _dbContext.Lesson.AddAsync(newLesson);
        await _dbContext.SaveChangesAsync();
    }

    

    
}