﻿using EducationPlatformDataBase.Data;
using System;
using System.Threading.Tasks;
using EducationPlatformDataBase.Data.Enums;
using EducationPlatformDataBase.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace EducationPlatformDataBase
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            var contextFactory = new ContextFactory();
            var dbContext = contextFactory.CreateDbContext(args);
            try
            {
                await dbContext.Database.MigrateAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine("Не удалось мигрировать");
            }
            Initialization.Initialize(dbContext);

            var provider = new Provider(dbContext); 
            await provider.ShowAllCourses();
            
            bool endProgram = false;
            User currentUser = null;

            await provider.ShowUsers();
            while (!endProgram)
            {
                provider.ShowCommand();

                Console.Write("Введите команду:");
                var command = Console.ReadLine();
                int.TryParse(command, out int parseResult);
                var commandResult = (Command) parseResult;
                string login;
                
                switch (commandResult)
                {
                    case Command.FailParse:
                        Console.WriteLine("Введена некорретная команда. Повторите попытку");
                        break;
                    case Command.Registration:
                        Console.Write("Введите логин: ");
                        login = Console.ReadLine();
                        Console.Write("Введите имя: ");
                        var userName = Console.ReadLine();
                        var registrationResult = await provider.Registration(userName,login);
                        ShowRegistrationResult(registrationResult);
                        if (registrationResult == RegistrationResult.Success)
                            currentUser = await provider.Authorization(login);
                        break;
                    case Command.Authorization:
                        Console.Write("Введите логин: ");
                        login = Console.ReadLine();
                        currentUser = await provider.Authorization(login);
                        if (currentUser==null)
                            Console.WriteLine("Авторизацие не удалась");
                        break;
                    case Command.ShowMyCourses :
                        if (currentUser == null)
                            Console.WriteLine("Авторизуйтесь");
                        else
                            await provider.ShowMyCources(currentUser.Id);
                        break;
                    case Command.AddNewCourse:
                        if (currentUser == null)
                            Console.WriteLine("Авторизуйтесь");
                        else
                            await provider.AddCourse(currentUser);
                        break;
                    case Command.AddNewParagraph:
                        if (!await provider.CanAddPagraph(currentUser))
                        {
                            Console.WriteLine("Для добавления раздела у вас должен быть созданный курс");
                            break;
                        }
                        if (currentUser == null)
                            Console.WriteLine("Авторизуйтесь");
                        else
                            await provider.AddParagraph(currentUser);
                        break;
                    case Command.AddNewLessons:
                        if (!await provider.CanAddLesson(currentUser))
                        {
                            Console.WriteLine("Для добавления урока у вас должен быть созданный курс и создан хотя бы один параграф");
                            break;
                        }
                        if (currentUser == null)
                            Console.WriteLine("Авторизуйтесь");
                        else
                            await provider.AddLesson(currentUser);
                        break;
                    case Command.ShowUsers:
                        await provider.ShowUsers();
                        break;
                    case Command.ShowAllCources:
                        await provider.ShowAllCourses();
                        break;
                    case Command.ShowCommand:
                        provider.ShowCommand();
                        break;
                    case Command.EndExecuting:
                        endProgram = true;
                        break;
                    default:
                        Console.WriteLine("Введена некорретная команда. Повторите попытку");
                        break;
                }
            }
        }

       
        private static void ShowRegistrationResult(RegistrationResult res)
        {
            switch (res)
            {
                case RegistrationResult.Success:
                    Console.WriteLine("Регистрация завершена успешно");
                    break;
                case RegistrationResult.IncorrectData:
                    Console.WriteLine("Логин и имя пользоветля должны быть заполнены");
                    break;
                case RegistrationResult.NotUniqueLogin:
                    Console.WriteLine("Пользователь с таким логином уже существует");
                    break;
            }
        }
    }
}

   
//
// var builder = new ConfigurationBuilder();
// builder.SetBasePath(Directory.GetCurrentDirectory());
// builder.AddJsonFile("appsettings.json");
// var config = builder.Build();
// string connectionString = config.GetConnectionString("DefaultConnection");
//
// var optionsBuilder = new DbContextOptionsBuilder<ModuleDbContext>();
// var options = optionsBuilder
//     .UseNpgsql(connectionString)
//     .Options;