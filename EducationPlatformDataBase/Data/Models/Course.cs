﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EducationPlatformDataBase.Data.Models
{
    public class Course
    {
        [Key]
        public int Id { get; set; }
        [Required, MaxLength(128)]
        public string Name { get; set; }
        public string Description { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
        public List<Paragraph> Paragraphs{ get; set; }
    }
}
