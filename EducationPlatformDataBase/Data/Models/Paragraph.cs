﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationPlatformDataBase.Data.Models
{
    public class Paragraph
    {
        [Key]
        public int Id { get; set; }
        [Required, MaxLength(200)]
        public string Name { get; set; }
        public string Description { get; set; } 

        public int CourseId { get; set; }
        public Course Course { get; set; }

        public List<Lesson> Lessons { get; set; }
    }
}
