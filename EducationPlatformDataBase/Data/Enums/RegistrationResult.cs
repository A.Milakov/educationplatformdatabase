﻿namespace EducationPlatformDataBase.Data.Enums;

public enum RegistrationResult
{
    Success,
    IncorrectData,
    NotUniqueLogin
}