namespace EducationPlatformDataBase.Data.Enums;
public enum Command
{
    FailParse ,
    
    Registration,
    Authorization,
    ShowMyCourses,
    
    AddNewCourse ,
    AddNewParagraph ,
    AddNewLessons ,
    ShowAllCources,
    ShowUsers,
    
    ShowCommand,    
    EndExecuting 
};