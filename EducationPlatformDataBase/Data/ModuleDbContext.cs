﻿using EducationPlatformDataBase.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace EducationPlatformDataBase.Data
{
    public class ModuleDbContext : DbContext
    {
        public DbSet<User> User { get; set; } 
        public DbSet<Course> Course { get; set; } 
        public DbSet<Lesson> Lesson { get; set; }
        public DbSet<Paragraph> Paragraph { get; set; }
        public ModuleDbContext(DbContextOptions<ModuleDbContext> options): base(options)
        {
        }
    }
}
