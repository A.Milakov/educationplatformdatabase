﻿using EducationPlatformDataBase.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace EducationPlatformDataBase.Data
{
    public static class Initialization
    {
        public static bool Initialize(ModuleDbContext dbContext)
        {
             try
             {
                 // проверка, была ли ранее инициализована база
                 if (dbContext.Course.Any())
                     return true;

                 var firstUser = new User
                 {
                     Name = "Ivan Ivanov" ,
                     Login = "i.ivanov"
                 };
                 var secondUser = new User
                 {
                     Name = "Petr Petrov",
                     Login = "p.petrov"
                 };
                 var semenUser = new User
                 {
                     Name = "Semyon Semenov",
                     Login = "s.semenov"
                 };
                 var sanUser = new User
                 {
                     Name = "San Sanych",
                     Login = "s.sanych"
                 };
                 var testUser = new User
                 {
                     Name = "Test User",
                     Login = "t.test"
                 };

                 var sharpBeginCourse = new Course()
                 {
                     Name = "C# Basic",
                     User = firstUser
                 };
                 var sharpProffCourse = new Course
                 {
                     Name = "C# Professional",
                     User = firstUser,
                     Description = "Вы сможете разрабатывать на C# сложные клиент-серверные приложения, используя паттерны проектирования и возможности CLR (многопоточность, асинхронность, рефлексия, сериализация) и LINQ.",
                 };
                 var jsBasicCourse = new Course()
                 {
                     Name = "Js Basic",
                     User = secondUser
                 };
                 var jsProffCouse = new Course()
                 {
                     Name = "Js Professional",
                     User = secondUser
                 };
                 var reactCourse = new Course()
                 {
                     Name = "React Developer",
                     User = secondUser
                 };

                 var sharpBasicFirstParagraph = new Paragraph()
                 {
                     Name = "ООП",
                     Course = sharpBeginCourse
                 };
                 var sharpBasicSecondParagraph = new Paragraph()
                 {
                     Name = "Алгоритмы",
                     Course = sharpBeginCourse
                 };
                 var oopLesson = new Lesson()
                 {
                     Name = "Наследование, Полиморфизм и Абстракция",
                     Paragraph = sharpBasicFirstParagraph
                 };
                 var interfaceLesson = new Lesson()
                 {
                     Name = "Интерфейсы",
                     Paragraph = sharpBasicFirstParagraph
                 };

                 var algLesson = new Lesson()
                 {
                     Name = "Циклы и рекурсия",
                     Paragraph = sharpBasicSecondParagraph
                 };
                 var heapTreeLesson = new Lesson()
                 {
                     Name = "Деревья и кучи",
                     Paragraph = sharpBasicSecondParagraph
                 };
                 var firstParagraph = new Paragraph
                 {
                     Name = "Архитектура проекта и Базы данных",
                     Description = string.Empty,
                     Course = sharpProffCourse,
                 };
                 var secondParagraph = new Paragraph
                 {
                     Name = "Клиент-серверная архитектура и микросервисы",
                     Course = sharpProffCourse,
                 };

                 var firstLesson = new Lesson
                 {
                        
                     Name = "Архитектура проекта",
                     Paragraph = firstParagraph,
                 };
                 var secondLesson = new Lesson
                 {
                     Name = " Базы данных: организация работы с потоками данных",
                     Paragraph = firstParagraph,
                 };
                 
                 var thirdLesson = new Lesson
                 {
                     Name = "WCF, ASMX, Web Api, REST",
                     Paragraph = secondParagraph,
                 };
                 var fourthLesson = new Lesson
                 {
                     Name = "Интеграция приложений",
                     Paragraph = secondParagraph,
                 };

                 dbContext.User.AddRange(firstUser, secondUser, semenUser,sanUser,testUser);
                 dbContext.Course.AddRange(sharpBeginCourse, sharpProffCourse, jsBasicCourse, jsProffCouse,
                     reactCourse);
                 dbContext.Paragraph.AddRange(firstParagraph, secondParagraph, sharpBasicFirstParagraph,
                     sharpBasicSecondParagraph);
                 dbContext.Lesson.AddRange(firstLesson, secondLesson, thirdLesson, fourthLesson, oopLesson, 
                     algLesson, interfaceLesson, heapTreeLesson);
                 dbContext.SaveChanges();
                 return true;
             }
             catch (Exception ex)
             {
                 Console.WriteLine(ex.ToString());
                 return false;
             }
        }
        public static bool Initialize(DbContextOptions<ModuleDbContext> options)
        {
            using ModuleDbContext db = new ModuleDbContext(options);
            return Initialize(db);
        }

    }
}
