﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace EducationPlatformDataBase.Data;

public class ContextFactory : IDesignTimeDbContextFactory<ModuleDbContext>
{
    public ModuleDbContext CreateDbContext(string[] args)
    {
        var optionsBuilder = new DbContextOptionsBuilder<ModuleDbContext>();
        ConfigurationBuilder builder = new ConfigurationBuilder();
        builder.SetBasePath(Directory.GetCurrentDirectory());
        builder.AddJsonFile("appsettings.json");

        IConfigurationRoot config = builder.Build();
        
        string connectionString = config.GetConnectionString("DefaultConnection");
        optionsBuilder.UseNpgsql(connectionString,
            opts => opts.CommandTimeout((int) TimeSpan.FromMinutes(10).TotalSeconds));
        return new ModuleDbContext(optionsBuilder.Options);
    }
}